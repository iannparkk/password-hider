
def main():
    print("""
----------------------------
Password Hider Maker Thingy
----------------------------
""")
    password_variable = input('What is the password variable: ')
    password_prompt = input('What is the prompt: ')
    the_password = input('What is the password: ')
    password_mask = input('What is the password mask: ')
    print('\n\n(Copy the following code)')
    print(f"""----------------------------
import stdiomask

{password_variable} = stdiomask.getpass(prompt = '{password_prompt}', mask = '{password_mask}')
if {password_variable} != '{the_password}':
    print('LOGIN FAILED')
else:
    print('LOGIN CONFIRMED')

----------------------------
""")

main()
input('Press [Enter] to exit')
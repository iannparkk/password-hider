When you prompt the user for password input, it's never a good idea to show it in plain text. 
You can use this program in order to create a mask for the characters the user types.

Example:
(Before this program) Password: chickensaregood
(After) Password: ***************